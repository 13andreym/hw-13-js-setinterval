const btnStop = document.querySelector('.stop')

const btnGo = document.querySelector('.go')



let imgIndex = 0;
moveImages();

let interval = setInterval(moveImages, 3000);

btnStop.addEventListener("click", () => {
  clearInterval(interval);
  btnGo.removeAttribute("disabled");
  btnStop.setAttribute("disabled", true);
});
btnGo.addEventListener("click", () => {
  interval = setInterval(moveImages, 3000);
  btnGo.setAttribute("disabled", true);
  btnStop.removeAttribute("disabled");
});


function moveImages() {
    let i;
    const images = document.querySelectorAll('.image-to-show');

    for (i = 0; i < images.length; i++) {
        images[i].classList.add("hidden");
    }
imgIndex++;
  if (imgIndex > images.length) imgIndex = 1;
  images[imgIndex - 1].classList.remove("hidden");

}
